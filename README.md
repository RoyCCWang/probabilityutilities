ProbabilityUtilities.jl
========


# Install
Steps to install this package in Julia:
1. start Julia REPL, then press the ```]``` key on your keyboard to enter package command mode.
2. Run the command ```add "https://gitlab.com/RoyCCWang/probabilityutilities"```
3. Run the command ```import ProbabilityUtilities``` to compile the package into cache storage.

If this repository is modified in the future, one needs to enter package mode in Julia REPL, and run the command ```update ProbabilityUtilities```.
