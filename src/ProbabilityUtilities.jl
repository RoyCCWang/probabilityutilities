module ProbabilityUtilities

using LinearAlgebra

import Distributions
import HCubature
import SpecialFunctions

import Utilities

include("drawRV.jl")
include("probability.jl")
include("probability_metrics.jl")
include("synthetic_densities.jl")

        # drawRV.jl
export  drawfromuniformindexinterval,
        probit,
        Gaussiancopula,
        evalGaussiancopula,
        drawstdnormalcdf,
        drawstdnormalcdf!,
        reservoirsamplingL,
        drawcategorical,

        # probability.jl
        getnormalizedensity,
        evalstdnormalcdf,
        evalstdnormalpdf,
        getMVNmarginalparams,

        # probability_metrics.jl
        evalKLintegral,

        # synthetic_densities.jl
        generaterandomGMM,
        getGMMdist

end # module
