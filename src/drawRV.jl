## routines that relate to the generation of random objects.

"""
    reservoirsamplingL(k::Int, n::Int)::Vector{Int}

Reservoir sampling: Algorithm L.
n is the number of entries in some sequence S.
Example:
N = 5000
X = randn(N)
k = 1

### uniformity test.
Nq = 10000*N
Y = collect( reservoirsamplingL(k,N)[1] for i = 1:Nq )

u = collect( count( Y.== j) for j = 1:N )

import Statistics
m = Statistics.mean(u)
v = Statistics.var(u)
"""
function reservoirsamplingL(k::Int,
                            n::Int)::Vector{Int} where T <: Real
    @assert n >= 1
    @assert 1 <= k <= n

    # set up.


    # initialize.
    R::Vector{Int} = collect( i for i = 1:k )

    # set up.
    w::Float64 = exp(log(rand())/k)
    u::Vector{Float64}  = ones(Float64, k) .* 1/k
    j::Int = 0
    tmp::Float64 = 0.0

    i = 1
    while i <= n
        tmp = log(rand())/log(1-w)
        i = i + floor(Int, tmp) + 1

        if i <= n
            j = drawcategorical(u)

            R[j] = i
            w = w * exp(log(rand())/k)
        end
    end

    return R
end


function drawcategorical(w::Vector{T})::Int where T <: Real
    x = rand(T)

    tmp::T = zero(T)
    for k = 1:length(w)-1
        if x < w[k]+tmp
            return k
        end
        tmp += w[k]
    end

    return length(w)
end
# #test:
# γ0 = 3.0
# w0_dist = Distributions.Dirichlet(collect( γ0/K_HDP for k = 1:K_HDP ))
# w0 = rand(w0_dist)
#
# N_samples = 200000
# X = collect( drawcategorical(w0) for n = 1:N_samples)
# w0_rec = collect( count(X.==k)/N_samples for k = 1:length(w0) )
# println("w0 is ", w0)
# println("w0_rec is ", w0_rec)

"""
    drawcategorical(w::Vector{T}, N::Int)::Vector{Int}

Draw N independent samples from the categorical distribution that is associated with w.
No error checking on w. It should sum to 1.
"""
function drawcategorical(w::Vector{T}, N::Int)::Vector{Int} where T <: Real

    𝑖 = Vector{Int}(undef,N)
    for n = 1:N
        𝑖[n] = drawcategorical(w)
    end

    return 𝑖
end


function drawnormal(μ::T, σ::T)::T where T <: Real
    return randn()*σ + μ
end

# draw isotropic normal.
function drawnormal(μ::Vector{T}, σ::T)::Vector{T} where T <: Real
    return collect( randn()*σ + μ[d] for d = 1:length(μ) )
end


# returns integer vector of K elements, each randomly picked (with replacement) in 1:1:N
# this is drawing K IID realizations from a uniform PMF from with support 1:1:N
function drawfromuniformindexinterval(N::Int, K::Int)
    a = rand(K)*N;
    return int(ceil(a));
end


### copula.

function probit(p::T)::T where T <: Real
    @assert zero(T) <= p <= one(T)
    return sqrt(2)*SpecialFunctions.erfinv(2*p-one(T))
end

function Gaussiancopula(u, Σ::Matrix{T}) where T <: Real
    N = length(u)
    @assert size(Σ) == (N,N)
    I_mat = LinearAlgebra.Diagonal{T}(LinearAlgebra.I, N)

    if all(isfinite.(u))
        return exp(-0.5*LinearAlgebra.dot(u,(inv(Σ)-I_mat)*u))/sqrt(LinearAlgebra.det(Σ))
    end

    return zero(T)
end

### TODO this should not use Distributions.jl
function evalGaussiancopula(  x,
                            R::Matrix{T},
                            marginal_dists) where T <: Real
    D = length(x)

    PDF_eval_x = collect( Distributions.pdf(marginal_dists[d], x[d]) for d = 1:D )
    probit_CDF_eval_x = collect( probit( Distributions.cdf(marginal_dists[d], x[d]) ) for d = 1:D )
    CDF_eval_x = collect( Distributions.cdf(marginal_dists[d], x[d]) for d = 1:D )


    #println(probit_CDF_eval_x)
    out = Gaussiancopula(probit_CDF_eval_x,R)*prod(PDF_eval_x)

    if isfinite(out) != true
        println(prod(PDF_eval_x))
        println(probit_CDF_eval_x)
        println(CDF_eval_x)
        println(x)
    end
    @assert isfinite(out) == true
    return out
end



### unused.

# algorithm 4.3 from Simulating Copulas
function drawfromGaussiancopula(inv_cdfs::Vector,
                                R::Matrix{T}) where T <: Real
    #
    D = size(R)[1]

    L = cholesky(R).L
    z = collect( randn() for d = 1:D )
    x = L*z
    for i = 1:D
        x[i] = inv_cdfs[i](inverseprobit(x[i]))
    end

    return x
end

function draw2D𝓝copulaγmarginals(  shape_parameters::Vector{T},
                                    scale_parameters::Vector{T},
                                    N::Int) where T <: Real
    #
    @assert length(shape_parameters) == length(scale_parameters)

    inv_cdfs = collect( xx->GSL.cdf_gamma_Pinv(xx, shape_parameters[d], scale_parameters[d]) for d = 1:D )
    X = collect( drawfromGaussiancopula(inv_cdfs, R) for n = 1:N )

    return X
end


####


# cdf( randn() ), cdf is that of the standard normal.
function drawstdnormalcdf()::Float64
    return 0.5*( 1.0 + SpecialFunctions.erf(randn()/sqrt(2)) )
end

function drawstdnormalcdf!(x::Vector{T}) where T <: Real
    for i = 1:length(x)
        x[i] = convert(T, drawstdnormalcdf())
    end

    return nothing
end
